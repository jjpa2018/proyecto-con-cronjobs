<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">

        <!-- Styles -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('template/assets/img/favicons/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('template/assets/img/favicons/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('template/assets/img/favicons/favicon-16x16.png') }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('template/assets/img/favicons/favicon.ico') }}">
        <link rel="manifest" href="{{ asset('template/assets/img/favicons/manifest.json') }}">
        <meta name="theme-color" content="#ffffff">
        <script src="{{ asset('template/assets/js/config.js') }}"></script>
        <script src="{{ asset('template/vendors/overlayscrollbars/OverlayScrollbars.min.js') }}"></script>

        <!-- ===============================================-->
        <!--    Stylesheets-->
        <!-- ===============================================-->
        <link rel="preconnect" href="https://fonts.gstatic.com/">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
        <link href="{{ asset('template/vendors/overlayscrollbars/OverlayScrollbars.min.css') }}" rel="stylesheet">
        <link href="{{ asset('template/assets/css/theme-rtl.min.css') }}" rel="stylesheet" id="style-rtl">
        <link href="{{ asset('template/assets/css/theme.min.css') }}" rel="stylesheet" id="style-default">
        <link href="{{ asset('template/assets/css/user-rtl.min.css') }}" rel="stylesheet" id="user-style-rtl">
        <link href="{{ asset('template/assets/css/user.min.css') }}" rel="stylesheet" id="user-style-default">
        <script>
        var isRTL = JSON.parse(localStorage.getItem('isRTL'));
        if (isRTL) {
            var linkDefault = document.getElementById('style-default');
            var userLinkDefault = document.getElementById('user-style-default');
            linkDefault.setAttribute('disabled', true);
            userLinkDefault.setAttribute('disabled', true);
            document.querySelector('html').setAttribute('dir', 'rtl');
        } else {
            var linkRTL = document.getElementById('style-rtl');
            var userLinkRTL = document.getElementById('user-style-rtl');
            linkRTL.setAttribute('disabled', true);
            userLinkRTL.setAttribute('disabled', true);
        }
        </script>

        @livewireStyles
        
    </head>
    <body class="font-sans antialiased bg-light">
        <x-jet-banner />
        @livewire('navigation-menu')

        <!-- Page Heading -->
        <header class="d-flex py-3 bg-white shadow-sm border-bottom">
            <div class="container">
                {{ $header }}
            </div>
        </header>

        <!-- Page Content -->
        <main class="container my-5">
            {{ $slot }}
        </main>

        @stack('modals')

        <!-- ===============================================-->
        <!--    JavaScripts-->
        <!-- ===============================================-->
        <script src="{{ asset('template/vendors/popper/popper.min.js')"></script>
        <script src="{{ asset('template/vendors/bootstrap/bootstrap.min.js')"></script>
        <script src="{{ asset('template/vendors/anchorjs/anchor.min.js')"></script>
        <script src="{{ asset('template/vendors/is/is.min.js')"></script>
        <script src="{{ asset('template/vendors/fontawesome/all.min.js')"></script>
        <script src="{{ asset('template/vendors/lodash/lodash.min.js')"></script>
        <script src="{{ asset('template/vendors/list.js/list.min.js')"></script>
        <script src="{{ asset('template/assets/js/theme.js')"></script>
        @livewireScripts

        @stack('scripts')
    </body>
</html>