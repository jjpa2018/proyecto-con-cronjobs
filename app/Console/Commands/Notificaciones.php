<?php

namespace App\Console\Commands;

use App\Extensions\ConsultasNotificaciones;
use App\Extensions\Curls;
use Illuminate\Console\Command;

class Notificaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:notificaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activa notificaciones en el bot Nora';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = ConsultasNotificaciones::saberPrimeraDosis();
        return Curls::notificaDiaprevioVacuna($data);
    }
}
