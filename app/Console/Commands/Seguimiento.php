<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Extensions\Curls;
use App\Mail\Notifications;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Seguimiento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:seguimiento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prueba de seguimientos en Nora';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Mail::to('jeferson.dev.web@gmail.com')->send(new Notifications);
    }
}
