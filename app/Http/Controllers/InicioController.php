<?php

namespace App\Http\Controllers;

use App\Extensions\ConsultasNotificaciones;
use App\Extensions\Curls;
use App\Http\Controllers\Controller;
use App\Mail\Notifications;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Mail;

class InicioController extends Controller
{
    public function ver()
    {
        //$data = ConsultasNotificaciones::saberPrimeraDosis();
        //return Curls::notificaDiaprevioVacuna($data);
        return Curls::getUsersNotification();
    }
}
