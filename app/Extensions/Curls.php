<?php

namespace App\Extensions;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use GuzzleHttp\Client;

class Curls
{

/**
     *
     *
     * @param
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getUsersNotification()
    {

        try {

            $client = new Client([
                'base_uri' => env('API_CUADRO_MANDO')
            ]);

            $body = [
                'cuestionario'       => 'c0'
            ];

            $res = $client->request('GET', '/api/cuadromando/cuestionario', [
                'auth' => ['mando_nora', 'mando_4p1n0r4'],
                'headers' => ['Content-Type' => 'application/json'],
                'body' => json_encode($body),
            ]);

            if ($res && $res->getStatusCode() == '200') {
                $res = (json_decode($res->getBody()->getContents()));
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }

    public static function notificaDiaprevioVacuna($number)
    {
        $variable = "[{'url':'https://now.silice.si/temp/evertec/recibo.pdf','tipo':'cliente','monto':'$2342'}]";
        $usernamelogin ="cl_notificaB0t";
        $passwordlogin ="cl_notificaB0t";
        $telefono_canal="+56998354717";
        $username ="cl_notificaB0t";
        try {

            $client = new Client([
                'base_uri' => env('API_REST_NORA_NOTIFICACION')
            ]);
    
            $body = [
                'contacts'       => $number,
                'usernamelogin'        => $usernamelogin,
                'passwordlogin'        => $passwordlogin,
                'telefono_canal' => $telefono_canal,
                'username'    => $username,
                'estado'  => '1_notif_1_cuponPDF',
                'SetVariables'=>$variable
            ];

            $res = $client->request('POST', '/api/notificacion', [
                'verify' => false,
                'headers' => [
                                'Content-Type' => 'application/json',
                                'auth' => ['silicepau', 'Silice2021']
                            ],
                'body' => json_encode($body),
            ]);
            
            if ($res && $res->getStatusCode() == '200') {
                $res = (json_decode($res->getBody()->getContents()));
                return $res;
            } else {
                return "nada";
            }

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $res = (json_decode($e->getResponse()->getBody()->getContents()));
            return $res;
        }
    }
}