<?php

namespace App\Extensions;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ConsultasNotificaciones
{

    public static function saberPrimeraDosis()
    {
        DB::statement("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");
        $listadoBD=DB::table('c0-bot as c')
                    ->select('p.conector_pau','p.telefono','c.respuesta')
                    ->join('persona as p','p.id','=','c.idpersona')
                    ->where('c.cod_preg_encuesta','C0.3')
                    ->groupBy('p.telefono')
                    ->get();
        
        $conector_pau=$listadoBD[0]->conector_pau;
        $telefono=$listadoBD[0]->telefono;
        $fecha=str_replace('/','-',$listadoBD[0]->respuesta);
        $fecha = date_create($fecha);
        $fecha = date_format($fecha,"d-m-Y");

        $dateNow = Carbon::now();
        $dateNow = $dateNow->format('d-m-Y');
        
        $fechaBD = Carbon::parse($dateNow);
        $fechaHoy = Carbon::parse($fecha);

        $diasDiferencia = $fechaBD->diffInDays($fechaHoy);

        if(($dateNow < $fecha) && $diasDiferencia == 1)
        {
            return $telefono;
        }           
    }

}
